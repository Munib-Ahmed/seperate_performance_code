#include <ostream>
#include <sstream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <iostream>

#include "timer.h"
#include "NanoLogCpp17.h"
using namespace NanoLog::LogLevels;
#define ALWAYS_INLINE inline __attribute__((always_inline))

class testing
{
private:
    std::string::iterator _str_itr;
    std::string _double_str;
    timer _timer;
    int _measurement_counter = 0;

public:
    void number_generator()
    {
        srand(time(0));

        for (int x = 0; x < 12500; x++)
        {
            std::cout << double(rand()) << std::endl;
            // double_to_str((double(rand())));
            // double_to_str((double(rand()) / double((RAND_MAX)) * 10.0));
        }
    }

    ALWAYS_INLINE std::string &double_to_str(const double &double_value) // TODO: Check AlwaysInline performance effect
    {
        std::stringstream _double_str_stream;

        if ((double_value - int64_t(double_value)) == 0) // TODO: Look into issue of limits
        {
            _double_str_stream << double_value << ".0";
            _double_str = _double_str_stream.str();
        }
        else
        {
            _double_str_stream << double_value;
            _double_str = _double_str_stream.str();

            _timer.start();
            _str_itr = (_double_str.end() - 1);
            if (*_str_itr == '0')
            {
                *_str_itr = '\0';
            }
            _timer.stop();

            double _time_duration = _timer.get_elapsed_ns();
            NANO_LOG(NOTICE, "Duration: *%0.0f", _time_duration);
        }

        return _double_str;
    }
};

int main()
{

    NanoLog::setLogFile("./performance-logs");
    NanoLog::preallocate();
    NanoLog::setLogLevel(NanoLog::NOTICE);
    testing test_object;

    test_object.number_generator();
    return 0;
}